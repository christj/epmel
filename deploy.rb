require "colorize"
require "popen4"
require "fileutils"
require "pathname"

$stdout.sync = true

# Create pub directory and copy RPMs
FileUtils::rm_rf './pub'
FileUtils::mkdir_p './pub'
FileUtils.cp Dir.glob('./*.rpm'), './pub'

# Create yum repo
outputs = ''
errors = ''
command_status = POpen4::popen4("createrepo -d pub")  do |stdout, stderr, stdin|
  stdout.each do |line|
    outputs << "#{line.strip}\n"
  end
  stderr.each do |line|
    errors << "#{line.strip}\n"
  end
end

#Create directory listings
def render_html(path)
  if File.directory?(path)
    pn = Pathname.new(path)
    file = File.open("#{pn}/index.html", "a")
    file.write("<html>\n")
    file.write("  <head>\n")
    file.write("   <title>EPMEL</title>\n")
    file.write("  </head>\n")
    file.write("  <body>\n")
    file.write("    <ul>\n")
    Dir.glob("#{pn}/*") do | subpath |
      spn = Pathname.new(subpath)
      #puts spn.parent
      file.write("      <li><a href='#{spn.basename}'>#{spn.basename}</a></li>\n")
      #puts "<li><a href='#{spn.basename}'>#{spn.basename}</a></li>"
      render_html(subpath)
    end
    file.write("    </ul>\n")
    file.write("  </body>\n")
    file.write("</html>\n")
    file.close
  end
end

render_html('./pub')