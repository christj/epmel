FROM centos:latest
MAINTAINER epmel <grainger@gmail.com>
RUN yum -y install epel-release
RUN yum -y update
RUN yum -y install rubygems
RUN yum -y install ruby-devel
RUN yum -y install rpm-build 
RUN yum -y install gcc 
RUN yum -y install gcc-c++ 
RUN yum -y install make 
RUN yum -y install libpcap-devel
RUN yum -y install wget
RUN yum -y install createrepo
RUN yum -y install perl-App-cpanminus
RUN yum -y install perl-local-lib
RUN yum -y install perl-CPAN
RUN yum -y install perl-MailTools
RUN yum -y install perl-Module-Build-Tiny
RUN yum -y install perl-WWW-Curl
RUN yum -y install perl-XML-Parser
RUN yum -y install perl-XML-LibXML
RUN yum -y install git
RUN yum -y install npm
RUN yum -y install rpm-sign
RUN gem install bundler --no-rdoc --no-ri
#WORKDIR /root/
#COPY Gemfile* /root/
#RUN bundle install --system
RUN npm install -g bower